<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = "LoginController";
$route['404_override'] = '';
$route['login']="LoginController";
$route['logout']="LoginController/logoutuser";
$route['register']="LoginController/checklogin";
$route['verify/(:num)'] = "LoginController/checklogin/$1";

$route['dashboard']="AdminController";
$route['employ']="EmployController";



$route['company']="CompanyController";
$route['companyc']="CompanyController/create_company";
$route['companyv']="CompanyController/create_company";
$route['companyv/(:num)']="CompanyController/view_company";
$route['companyu/(:num)']="CompanyController/update_company";
$route['companyd/(:num)']="CompanyController/delete_company";
$route['companyr']="CompanyController/companiesrecords";


$route['shop']="ShopController";
$route['shopc']="ShopController/create_shop";
$route['shopr']="ShopController/shoprecords";
$route['shopv/(:num)']="ShopController/shopview";
$route['shopu/(:num)']="ShopController/shopupdates";
$route['shopd/(:num)']="ShopController/deleteshop";





$route['category']="CategoryController";
$route['categoryc']="CategoryController/create_category";
$route['categoryr']="CategoryController/category_records";
$route['categoryu/(:num)']="CategoryController/category_update";
$route['categoryv/(:num)']="CategoryController/category_view";
$route['categoryd/(:num)']="CategoryController/category_delete";


$route['product']="ProductController";
$route['productc']="ProductController/create_product";
$route['productr']="ProductController/product_records";
$route['productv/(:num)']="ProductController/product_view";
$route['productu/(:num)']="ProductController/product_update";
$route['productd/(:num)']="ProductController/product_delete";








$route['employes']="EmployesController";
$route['employesv/(:num)']="EmployesController/employee_view";
$route['activeemp/(:num)']="EmployesController/employ_activate";
$route['blockedemp/(:num)']="EmployesController/employ_blocked";
$route['deleteemp/(:num)']="EmployesController/employ_delete";
$route['createemp']="EmployesController/create_employ";
$route['updateemp/(:num)']="EmployesController/employprofile_update";
$route['employeement']="EmployesController/joiningshop";

$route['dealer']="DealerController";
$route['dealerc']="DealerController/create_dealer";
$route['dealerv/(:num)']="DealerController/dealer_view";
$route['dealeru/(:num)']="DealerController/update_dealer";
$route['dealerd/(:num)']="DealerController/delete_dealer";


$route['customerc']="CustomerController/customer_create";
$route['customerv/(:num)']="CustomerController/customerview";
$route['customeru/(:num)']="CustomerController/customer_update";
$route['customerd/(:num)']="CustomerController/customer_delete";
$route['customer']="CustomerController";

$route['buynsalec']="BuynsaleController/create_buynsale";
$route['buynsale']="BuynsaleController";
$route['buynsalev/(:num)']="BuynsaleController/order_view";
$route['buynsaled/(:num)']="BuynsaleController/order_delete";


$route['bankdetailc']="BankDetailsController/add_bankdetails";
$route['bankdetail']="BankDetailsController";
$route['bankdetailv/(:num)']="BankDetailsController/viewbankdetails";
$route['bankdetailu/(:num)']="BankDetailsController/update_bankdetails";
$route['bankdetaild/(:num)']="BankDetailsController/delete_bank";
//$route['getemploy']="EmployesController/employinfo";
/* End of file routes.php */
/* Location: ./application/config/routes.php */